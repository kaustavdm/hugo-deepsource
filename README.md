# Hugo DeepSource starter

This is a starter template for integrating a [Hugo](https://gohugo.io) repository in [Bitbucket](https://bitbucket.org) with [DeepSource](https://deepsource.io) for continuous code analysis.

This repository was generated using `$ hugo new site hugo-deepsource`.